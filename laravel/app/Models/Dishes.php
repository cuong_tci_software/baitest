<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Dishes extends Model
{
    use HasFactory;

    public $timestamps = true;
    protected $fillable = [
        'id',
        'meal_id',
        'restaurant_id',
        'number_people'
    ];

    public function meal() 
    {
        return $this->belongsTo(Meal::class, 'meal_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');
    }


    public function dish()
    {
        return $this->belongsToMany(
            Dish::class,
            'available_meals',
            'dishes_id',
            'dish_id'
        )->withTimestamps();
    }
}