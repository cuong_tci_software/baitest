<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Dish extends Model
{
    use HasFactory;

    protected $table = 'dish';
    public $timestamps = true;
    protected $fillable = [
        'id',
        'name',
    ];
}