<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Meal extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $fillable = [
        'id',
        'name',
    ];
}