<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class AvailableMeals extends Model
{
    use HasFactory;

    public $timestamps = true;
    protected $fillable = [
        'id',
        'dishes_id',
        'dish_id'
    ];
}