<?php
namespace App\Services;
use App\Repositories\Meal\MealRepositoryInterface;

class MealService {
    /**
     *
     * @var MealRepositoryInterface
     */
    private $mealRepository;

    /**
     * MealService constructor.
     *
     * @param MealRepositoryInterface $mealRepository
     */
    public function __construct(MealRepositoryInterface $mealRepository)
    {
        $this->mealRepository = $mealRepository;
    }

    /**
     * @param $request
     */
    public function index($request)
    {
        return $this->mealRepository->getAll($request);
    }

}