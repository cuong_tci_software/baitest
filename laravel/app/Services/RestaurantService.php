<?php
namespace App\Services;
use App\Repositories\Restaurant\RestaurantRepositoryInterface;

class RestaurantService {
    /**
     *
     * @var RestaurantRepositoryInterface
     */
    private $restaurantRepository;

    /**
     * MealService constructor.
     *
     * @param RestaurantRepositoryInterface $restaurantRepository
     */
    public function __construct(RestaurantRepositoryInterface $restaurantRepository)
    {
        $this->restaurantRepository = $restaurantRepository;
    }

    /**
     * @param $request
     */
    public function index($request)
    {
        return $this->restaurantRepository->getAll($request);
    }

}