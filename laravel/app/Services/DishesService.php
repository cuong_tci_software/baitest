<?php
namespace App\Services;
use App\Repositories\Dishes\DishesRepositoryInterface;

class DishesService {
    /**
     *
     * @var DishesRepositoryInterface
     */
    private $dishesRepository;

    /**
     * DishesService constructor.
     *
     * @param DishesRepositoryInterface $dishesRepository
     */
    public function __construct(DishesRepositoryInterface $dishesRepository)
    {
        $this->dishesRepository = $dishesRepository;
    }

    /**
     * @param $request
     */
    public function index($request)
    {
        return $this->dishesRepository->getAll($request);
    }

    /**
     * @param $attr
     */
    public function create($attr)
    {
        return $this->dishesRepository->create($attr);
    }

}