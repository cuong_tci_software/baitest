<?php
namespace App\Services;
use App\Repositories\Dish\DishRepositoryInterface;

class DishService {
    /**
     *
     * @var DishRepositoryInterface
     */
    private $dishRepository;

    /**
     * DishService constructor.
     *
     * @param DishRepositoryInterface $dishRepository
     */
    public function __construct(DishRepositoryInterface $dishRepository)
    {
        $this->dishRepository = $dishRepository;
    }

    /**
     * @param $request
     */
    public function index($request)
    {
        return $this->dishRepository->getAll($request);
    }

}