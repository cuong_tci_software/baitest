<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Meal\MealRepository;
use App\Repositories\Meal\MealRepositoryInterface;
use App\Repositories\Restaurant\RestaurantRepository;
use App\Repositories\Restaurant\RestaurantRepositoryInterface;
use App\Repositories\Dish\DishRepository;
use App\Repositories\Dish\DishRepositoryInterface;
use App\Repositories\Dishes\DishesRepository;
use App\Repositories\Dishes\DishesRepositoryInterface;
/**
 * RepositoryServiceProvider
 */
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function register()
    {
        $repositories = [
            MealRepositoryInterface::class => MealRepository::class,
            RestaurantRepositoryInterface::class => RestaurantRepository::class,
            DishRepositoryInterface::class => DishRepository::class,
            DishesRepositoryInterface::class => DishesRepository::class
        ];

        foreach ($repositories as $interface => $repository) {
            $this->app->singleton($interface, $repository);
        }
    }
}