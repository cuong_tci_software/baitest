<?php

namespace App\Repositories\Dish;

interface DishRepositoryInterface
{
    public function getAll($data);
}