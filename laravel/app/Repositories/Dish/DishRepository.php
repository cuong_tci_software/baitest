<?php

namespace App\Repositories\Dish;

use App\Models\Dish;
use App\Repositories\BaseRepository;

class DishRepository extends BaseRepository implements DishRepositoryInterface
{
    public function __construct(Dish $model)
    {
        parent::__construct($model);
    }

    public function getAll($data)
    {
        return $this->model->get();
    }
}