<?php

namespace App\Repositories\Meal;

interface MealRepositoryInterface
{
    public function getAll($data);
}