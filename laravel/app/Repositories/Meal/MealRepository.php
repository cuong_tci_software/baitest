<?php

namespace App\Repositories\Meal;

use App\Models\Meal;
use App\Repositories\BaseRepository;

class MealRepository extends BaseRepository implements MealRepositoryInterface
{
    public function __construct(Meal $model)
    {
        parent::__construct($model);
    }

    public function getAll($data)
    {
        return $this->model->get();
    }
}