<?php

namespace App\Repositories\Restaurant;

interface RestaurantRepositoryInterface
{
    public function getAll($data);
}