<?php

namespace App\Repositories\Restaurant;

use App\Models\Restaurant;
use App\Repositories\BaseRepository;

class RestaurantRepository extends BaseRepository implements RestaurantRepositoryInterface
{
    public function __construct(Restaurant $model)
    {
        parent::__construct($model);
    }

    public function getAll($data)
    {
        return $this->model->get();
    }
}