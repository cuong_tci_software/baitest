<?php

namespace App\Repositories\Dishes;

use App\Models\Dishes;
use App\Repositories\BaseRepository;
use DB;

class DishesRepository extends BaseRepository implements DishesRepositoryInterface
{
    public function __construct(Dishes $model)
    {
        parent::__construct($model);
    }

    public function getAll($data)
    {
        return Dishes::with(['dish', 'meal', 'restaurant'])->get();
    }

    public function create($attr)
    {
        DB::beginTransaction();

        try {
            
            $dishList = $attr['dish'];
            unset($attr['dish']);

            $create = $this->model->create($attr);
            
            if($create && count($dishList) > 0) {
                $create->dish()->sync($dishList);
            } else {
                throw new Exception("Error server");
            }

            DB::commit();
            return $create;
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}