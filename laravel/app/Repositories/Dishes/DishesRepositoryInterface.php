<?php

namespace App\Repositories\Dishes;

interface DishesRepositoryInterface
{
    public function getAll($data);
    public function create($data);
}