<?php

namespace App\Enums;

final class MessageEnum
{
    const List_Meal = "List Meal";
    const List_Restaurant = "List Restaurant";
    const List_Dish = "List Dish";

}