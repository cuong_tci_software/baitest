<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DishesResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?? null,
            'name' => $this->meal->name??null,
            'restaurant' => $this->restaurant->name??null,
            'availableMeals' => $this->whenLoaded(
                'dish',
                $this->dish->pluck('name')->unique()->all(),
                []
            )
        ];
    }
}