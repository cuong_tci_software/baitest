import { createRouter, createWebHistory } from 'vue-router'

import DishesList from '@/components/Dishes/List.vue'
import DishesAdd from '@/components/Dishes/Add.vue'

const routes = [
    {
        path: '/',
        component: DishesList
    },
    {
        path: '/dishes-add',
        component: DishesAdd
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes: routes
})

export default router