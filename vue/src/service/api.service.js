import axios from 'axios'
import { ResponseWrapper, ErrorWrapper } from './util'
import Api from '../config/constants';

export class ApiService {
  /**
   ******************************
   * @API
   ******************************
   */

  static async getDishes () {
    try {
      const response = await axios.get(Api.LIST_DISHES)
      return new ResponseWrapper(response, response.data.data)
    } catch (error) {
      throw new ErrorWrapper(error)
    }
  }

  static async getMeals () {
    try {
      const response = await axios.get(Api.LIST_MEALS)
      return new ResponseWrapper(response, response.data.data)
    } catch (error) {
      throw new ErrorWrapper(error)
    }
  }

  static async getRestaurant () {
    try {
      const response = await axios.get(Api.LIST_RESTAURANT)
      return new ResponseWrapper(response, response.data.data)
    } catch (error) {
      throw new ErrorWrapper(error)
    }
  }

  static async getDish () {
    try {
      const response = await axios.get(Api.LIST_DISH)
      return new ResponseWrapper(response, response.data.data)
    } catch (error) {
      throw new ErrorWrapper(error)
    }
  }

  static async createDishes (requestData) {
    try {
      const response = await axios.post(Api.CREATE_DISHES, requestData)
      return new ResponseWrapper(response, response.data.data)
    } catch (error) {
      throw new ErrorWrapper(error)
    }
  }
}