export default {
    LIST_DISHES: 'dishes',
    LIST_MEALS: 'meals',
    LIST_RESTAURANT: 'restaurant',
    LIST_DISH: 'dish',
    CREATE_DISHES: 'dishes'
 }