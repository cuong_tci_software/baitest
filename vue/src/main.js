import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
//axios
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';

const app = createApp(App)
app.use(VueAxios, axios)
axios.defaults.baseURL = "http://localhost/api/"
app.use(router)
app.mount('#app')